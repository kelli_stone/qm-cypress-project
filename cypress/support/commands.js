// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

// Cypress.Commands.add("inject", {
    // var qtm = document.createElement('script'); qtm.type = 'text/javascript'; qtm.async = 1;
    // qtm.src = 'https://cdn.quantummetric.com/instrumentation/quantum-qa1.js';
    // var d = document.getElementsByTagName('script')[ 0 ];
    // !window.QuantumMetricAPI && d.parentNode.insertBefore(qtm, d);
// })

// window[ "QuantumMetricOnload" ] = function () {

//     var sessionID = QuantumMetricAPI.getSessionID();
//     // <VERIFY HERE THAT sessionID IS UNDEFINED>
// };

